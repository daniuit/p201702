show databases;
-- 查看数据库
create database school default character set utf8; 
-- 创建库
use school;
--使用库
show tables;
--显示库里面的表
drop database if exists school;
-- 删除库

-- 创建表时必须定义表结构

create table stu(
	id int(6) primary key AUTO_INCREMENT COMMENT "主键",
	name char(20) unique COMMENT '用户名',
	age int(3) unsigned default 20 COMMENT '年龄',
	sex enum('男','女','人妖') default "人妖" COMMENT '性别',
	qq char(11) not null COMMENT 'qq',
	phone char(11) COMMENT "电话",
	addr char(100) COMMENT "地址",
	email char(50) unique,
	password char(32)
);
desc stu;
-- 查看表结构

create table question(
	id int(6) primary key AUTO_INCREMENT COMMENT '主键' ,
	title char(100) not null COMMENT '标题',
	content text default '' COMMENT '内容',
	uid int(6) not null COMMENT '用户',
	cid int(4) not null COMMENT '分类',
	view_num int(6) default 0 COMMENT '浏览量',
	answer_num int(6) default 0 COMMENT '回复量',
	create_time int(10) COMMENT '创建时间'
);


insert into user (username,password,email,create_time) values('小明','123456','113@qq.com',now());