<?php 

function ReAjax($num,$info)
{
	$arr = array(
		'error'=>$num,
		'info'=>$info
	);
	echo json_encode($arr);exit;
}


/**
 * [Ctime 转时间格式函数]
 * @Author   Xuebingsi
 * @DateTime 2017-05-08T08:32:40+0800
 * @param    [int]                   $time [传入的时间戳]
 */
function Ctime($time=null)
{
	if($time){

		$now = time();

		$diff = $now-$time;

		if($diff<60){
			return "刚刚";
		}elseif($diff<3600){
			return round($diff/60)."分钟前";
		}elseif($diff<86400){
			return round($diff/3600)."小时之前";
		}elseif($diff<(86400*7)){
			return round($diff/86400)."天之前";
		}else{
			return date("Y-m-d H:i:s",$time);
		}
	}else{
		return "请传入想要转换的时间戳";
	}
}


function Sendmsg($phone,$name,$code)
{

	$ParamString = "{'name':'".$name."','num':'".$code."'}";

	$host = "http://sms.market.alicloudapi.com";
    $path = "/singleSendSms";
    $method = "GET";
    $appcode = "ab7bfa4f7d544241998ea5d8a8ddb028";
    $headers = array();
    array_push($headers, "Authorization:APPCODE " . $appcode);
    $querys = "ParamString=".$ParamString."&RecNum=".$phone."&SignName=学并思教育&TemplateCode=SMS_72205005";

    // var_dump($querys);exit;
    $bodys = "";
    $url = $host . $path . "?" . $querys;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_FAILONERROR, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    if (1 == strpos("$".$host, "https://"))
    {
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    $res = curl_exec($curl);

    return $res;
}





 ?>