<?php
namespace Index\Controller;
use Think\Controller;
class CommonController extends Controller {
    public function is_login()
    {
    	if(!isset($_SESSION['uid'])){
            $this->error('你还没有登录请先登录',U('index/login/index'));
        }
    }

    public function _initialize()
    {

    	$now = MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;

    	// var_dump($now);
    	
    	$paths = C('AUTH_PATH');

    	// var_dump($paths);

    	if(in_array($now, $paths)){
    		if(!isset($_SESSION['uid'])){
	            $this->error('你还没有登录请先登录',U('index/login/index'),0);
	        }
    	}
    }

    public function addview($id,$num)
    {
        $key = "view_num".$id;

        if(S($key)){
            $data = S($key);
            if($data>time()){
                $data['num']+=1;
                S($key,$data);
                return $data['num'];
            }else{
                $data['view_num'] = $data['num']+1;
                M('question')->where("id='$id'")->save($data);
                S($key,["exp"=>time()+1800,'num'=>$data['view_num']]);
                return $data['view_num'];
            }
        }else{
           $row =  M('question')->where("id='$id'")->find();
            S($key,["exp"=>time()+1800,'num'=>$row['view_num']+1]);
            return $row['view_num']+1;
        }

        // var_dump($key);

       //  if(S("view_num")){

       //  }else{

       //     S("view_num") 
       //  }
       // M('question')->where("id='$id'")->setInc('view_num');
    }
}