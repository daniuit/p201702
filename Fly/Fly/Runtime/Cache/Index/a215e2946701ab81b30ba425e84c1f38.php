<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>
            <?php echo $title ?>-<?php echo C('SITE_NAME') ?>
        </title>
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/font.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/layui.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/global.css">
        <script src="/P201702/Fly/Public/Index/layui.js"></script>
    </head>
    
    <body>
        <div class="header">
            <div class="main">
                <a class="logo" href="/P201702/Fly" title="Fly">
                    Fly社区
                </a>
                <div class="nav">
                    <a href="/jie/">
                        <i class="iconfont icon-wenda">
                        </i>
                        讨论
                    </a>
                    <a href="/case/2017/">
                        <i class="iconfont icon-iconmingxinganli" style="top: 2px;">
                        </i>
                        案例
                    </a>
                    <a href="http://www.layui.com/">
                        <i class="iconfont icon-ui">
                        </i>
                        框架
                    </a>
                </div>
                <?php if(isset($_SESSION['uid'])){ ?>
                    <div class="nav-user">
                    <a class="avatar" href="/user/">
                        <img id="face" src="<?php echo $_SESSION['face'] ?>">
                        <cite>
                            <?php echo $_SESSION['nickname'] ?>
                        </cite>
                    </a>
                    <div class="nav">
                        <a href="/user/set/">
                            <i class="iconfont icon-shezhi">
                            </i>
                            设置
                        </a>
                        <a href="<?php echo U('index/login/out');?>">
                            <i class="iconfont icon-tuichu" style="top: 0; font-size: 22px;">
                            </i>
                            退了
                        </a>
                    </div>
                </div>
                <?php }else{ ?>

               
                <div class="nav-user">
                    <a class="unlogin" href="<?php echo U('index/login/index');?>">
                        <i class="iconfont icon-touxiang">
                        </i>
                    </a>
                    <span>
                        <a href="<?php echo U('index/login/index');?>">
                            登入
                        </a>
                        <a href="<?php echo U('index/reg/index');?>">
                            注册
                        </a>
                    </span>
                    <p class="out-login">
                        <a href="<?php echo U('index/login/qqlogin');?>" 
                        class="iconfont icon-qq" title="QQ登入">
                        </a>
                        <a href="http://fly.layui.com:8098/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
                        class="iconfont icon-weibo" title="微博登入">
                        </a>
                    </p>
                </div>

                 <?php } ?>
                
            </div>
        </div>
        <div class="main layui-clear">
    <div class="wrap">
        <div class="content detail">
            <div class="fly-panel detail-box">
                <h1>
                    <?php echo $ques['title'] ?>
                </h1>
                <div class="fly-tip fly-detail-hint">
                    <span>
                        <?php echo $ques['status'] ?>
                    </span>
                    <div class="jie-admin-box" data-id="10476">
                    </div>
                    <div class="fly-list-hint">
                        <a href="#comment">
                            <i class="iconfont" title="回答">
                                &#xe60c;
                            </i>
                            <?php echo $ques['answer_num'] ?>
                        </a>
                        <i class="iconfont" title="人气">
                            &#xe60b;
                        </i>
                        <?php echo $ques['view_num'] ?>
                    </div>
                </div>
                <div class="detail-about">
                    <a class="jie-user" href="/u/5075280/">
                        <img src="/P201702/Fly<?php echo $ques['face'] ?>" alt="pacy">
                        <cite>
                            <?php echo $ques['nickname'] ?>
                            <em>
                                发布于<?php echo Ctime($ques['create_time']) ?>小时前
                            </em>
                        </cite>
                    </a>
                    <div class="detail-hits" id="LAY_jieAdmin" data-id="10476">
                        <span style="color:#FF7200">
                            悬赏：<?php echo $ques['kiss'] ?>飞吻
                        </span>
                        <?php if(isset($_SESSION['uid'])){ ?>
                            <?php if($ques['is_coll']){ ?>
                                <span qid="<?php echo $ques['id'] ?>" class="layui-btn layui-btn-mini jie-admin layui-btn-danger" id="collect" type="collect" data-type="add">取消收藏</span>
                            <?php }else{ ?>
                                 <span qid="<?php echo $ques['id'] ?>" class="layui-btn layui-btn-mini jie-admin " id="collect" type="collect" data-type="add">收藏</span>
                            <?php } ?>
                           
                        <?php } ?>
                    </div>
                </div>
                <div class="detail-body photos">
                    <?php echo htmlspecialchars_decode($ques['content']) ?>
                </div>
            </div>
            <div class="fly-panel detail-box" style="padding-top: 0;">
                <ul class="jieda photos" id="jieda">
                    <?php foreach ($answer as $row) { ?>
                        <li data-id="35957">
                            <a name="item-1496307640978">
                            </a>
                            <div class="detail-about detail-about-reply">
                                <a class="jie-user" href="/u/11424/">
                                    <img src="/P201702/Fly<?php echo $row['face'] ?>" alt="SMALL">
                                    <cite>
                                        <i>
                                            <?php echo $row['nickname'] ?>
                                        </i>
                                        <em style="padding:0 ; color: #FF7200;">
                                            <!-- VIP2 -->
                                        </em>
                                    </cite>
                                </a>
                                <div class="detail-hits">
                                    <span>
                                        <?php echo Ctime($row['create_time']) ?>
                                    </span>
                                </div>
                            </div>
                            <div class="detail-body jieda-body">
                                <?php echo htmlspecialchars_decode($row['content']) ?>
                            </div>
                            <div class="jieda-reply">
                                <span class="jieda-zan xbszan <?php if($row['is_zan']){echo 'zanok';} ?>" type="zan" aid="<?php echo $row['id'] ?>">
                                    <i class="iconfont icon-zan">
                                    </i>
                                    <em>
                                        <?php echo $row['zan_num'] ?>
                                    </em>
                                </span>
                                <span class="xbsreplay" nickname="<?php echo $row['nickname'] ?>" type="reply">
                                    <i class="iconfont icon-svgmoban53 ">
                                    </i>
                                    <a href="#comment">回复</a>
                                </span>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <div class="layui-form layui-form-pane">
                    <form action="/jie/reply/" method="post">
                        <div class="layui-form-item layui-form-text">
                            <a name="comment">
                            </a>
                            <div class="layui-input-block">
                                <textarea id="L_content" name="content" 
                                placeholder="我要回答" class="layui-textarea fly-editor" style="height: 150px;"></textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <input type="hidden" name="qid" value="<?php echo $ques['id'] ?>">
                            <button class="layui-btn" lay-filter="add" lay-submit>
                                提交回答
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="edge">
        <dl class="fly-panel fly-list-one">
            <dt class="fly-panel-title">
                最近热帖
            </dt>
            <?php foreach ($hotview as $row) { ?>
            <dd>
                <a href="<?php echo U('index/jie/index',array('id'=>$row['id']));?>">
                    <?php echo $row['title'] ?>
                </a>
                <span>
                    <i class="iconfont">
                        &#xe60b;
                    </i>
                     <?php echo $row['view_num'] ?>
                </span>
            </dd>
            <?php } ?>
        </dl>
        <dl class="fly-panel fly-list-one">
            <dt class="fly-panel-title">
                近期热议
            </dt>
            <dd>
                <a href="/jie/10355.html">
                    弹出层不能置顶！！
                </a>
                <span>
                    <i class="iconfont">
                        &#xe60c;
                    </i>
                    12
                </span>
            </dd>
        </dl>
    </div>
</div>
<script type="text/javascript">
    //一般直接写在一个js文件中
    layui.use(['layer','form','jquery','layedit'], function(){
      
      var layer = layui.layer;
      var form = layui.form();
      var $ = layui.jquery;
      var layedit = layui.layedit;
      //监听提交
      //
      $('#collect').click(function(event) {
          qid = $(this).attr('qid');
          $.ajax({
              url: '<?php echo U('index/jie/collect');?>',
              type: 'post',
              dataType: 'json',
              data: {qid: qid},
          })
          .done(function(respones) {
                if(respones.error==0){
                    $('#collect').html('取消收藏').addClass('layui-btn-danger');
                }else{
                    $('#collect').html('收藏').removeClass('layui-btn-danger');
                }
          })
          
      });

      $('.xbszan').click(function(event) {
          aid = $(this).attr('aid');

          obj = $(this);

          $.ajax({
              url: '<?php echo U('index/jie/zan');?>',
              type: 'post',
              dataType: 'json',
              data: {aid: aid},
          })
          .done(function(respones) {
                num = parseInt(obj.children('em').html());
                if(respones.error==0){
                   obj.addClass('zanok'); 
                   obj.children('em').html(num+1);
                }else{
                   obj.removeClass('zanok');
                   obj.children('em').html(num-1); 
                }
          })
      });

        layedit.set({
            uploadImage: {
            url: '<?php echo U('index/jie/upload');?>' //接口url
            ,type: '' //默认post
            }
        });

        index = layedit.build('L_content'); //建立编辑器


        //监听提交
        form.on('submit(add)', function(data){

            content = layedit.getContent(index);

            if(content.length<20){
                layer.msg("内容不能少于20长度");
                return false;
            }

            data.field.content=content;
            // console.log(data.field);
            //发异步
            $.ajax({
                url: '<?php echo U('index/jie/addanswer');?>',//异步地址
                type: 'POST',
                dataType: 'json',
                data: data.field,//发送的数据
            })
            .done(function(respones) {
                //失败
                if(respones.error==1){
                    // 把失败信息弹出
                    layer.msg(respones.info, function(){});
                }else{
                    //把成功信息弹出，并中转到登录页面
                    layer.alert(respones.info, {icon: 6},function  () {
                        location.reload();
                    })
                }
            })
            .fail(function() {
                console.log("error");
            })
            
            return false;
        });


        $('.xbsreplay').click(function(event) {
            content = layedit.getContent(index);
            nickname = $(this).attr('nickname');
            str ="@"+'<a href="/jump?username='+nickname+'" class="fly-aite" target="_blank">'+nickname+'&nbsp;</a>';
            content +=str;
            layedit.setContent(index,content);
            layedit.xbsfocus(index,content);
        });      

    });
</script>
        <div class="footer">
            <p>
                <a href="http://fly.layui.com/">
                    Fly社区
                </a>
                2017 &copy;
                <a href="http://www.layui.com/">
                    layui.com
                </a>
            </p>
            <p>
                <a href="http://fly.layui.com/jie/3147.html" target="_blank">
                    产品授权
                </a>
                <a href="http://fly.layui.com/jie/8157.html" target="_blank">
                    获取Fly社区模版
                </a>
                <a href="http://fly.layui.com/jie/2461.html" target="_blank">
                    微信公众号
                </a>
            </p>
        </div>
    </body>
</html>