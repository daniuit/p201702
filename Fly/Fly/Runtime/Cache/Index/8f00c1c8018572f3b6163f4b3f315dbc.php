<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>
            <?php echo $title ?>-<?php echo C('SITE_NAME') ?>
        </title>
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/font.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/layui.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/global.css">
        <script src="/P201702/Fly/Public/Index/layui.js"></script>
    </head>
    
    <body>
        <div class="header">
            <div class="main">
                <a class="logo" href="/P201702/Fly" title="Fly">
                    Fly社区
                </a>
                <div class="nav">
                    <a href="/jie/">
                        <i class="iconfont icon-wenda">
                        </i>
                        讨论
                    </a>
                    <a href="/case/2017/">
                        <i class="iconfont icon-iconmingxinganli" style="top: 2px;">
                        </i>
                        案例
                    </a>
                    <a href="http://www.layui.com/">
                        <i class="iconfont icon-ui">
                        </i>
                        框架
                    </a>
                </div>
                <?php if(isset($_SESSION['uid'])){ ?>
                    <div class="nav-user">
                    <a class="avatar" href="/user/">
                        <img id="face" src="<?php echo $_SESSION['face'] ?>">
                        <cite>
                            <?php echo $_SESSION['nickname'] ?>
                        </cite>
                    </a>
                    <div class="nav">
                        <a href="/user/set/">
                            <i class="iconfont icon-shezhi">
                            </i>
                            设置
                        </a>
                        <a href="<?php echo U('index/login/out');?>">
                            <i class="iconfont icon-tuichu" style="top: 0; font-size: 22px;">
                            </i>
                            退了
                        </a>
                    </div>
                </div>
                <?php }else{ ?>

               
                <div class="nav-user">
                    <a class="unlogin" href="<?php echo U('index/login/index');?>">
                        <i class="iconfont icon-touxiang">
                        </i>
                    </a>
                    <span>
                        <a href="<?php echo U('index/login/index');?>">
                            登入
                        </a>
                        <a href="<?php echo U('index/reg/index');?>">
                            注册
                        </a>
                    </span>
                    <p class="out-login">
                        <a href="<?php echo U('index/login/qqlogin');?>" 
                        class="iconfont icon-qq" title="QQ登入">
                        </a>
                        <a href="http://fly.layui.com:8098/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
                        class="iconfont icon-weibo" title="微博登入">
                        </a>
                    </p>
                </div>

                 <?php } ?>
                
            </div>
        </div>
        <div class="fly-home" style="background-image: url();">
            <img src="/P201702/Fly<?php echo $user['face'] ?>" alt="学并思">
            <h1>
                <?php echo $user['nickname'] ?>
                <i class="iconfont           icon-nan        ">
                </i>
            </h1>
            <p class="fly-home-info">
                <i class="iconfont icon-zuichun" title="飞吻">
                </i>
                <span style="color: #FF7200;">
                    <?php echo $user['kiss'] ?>
                </span>
                <i class="iconfont icon-shijian">
                </i>
                <span>
                    <?php echo Ctime($user['nickname']) ?>
                </span>
                <i class="iconfont icon-chengshi">
                </i>
                <span>
                    来自广州
                </span>
            </p>
            <p class="fly-home-sign">
                （专业的PHP培训班，以实战为教学导向，教授企业最实用技能。）
            </p>
        </div>
        <div class="main fly-home-main">
            <div class="layui-inline fly-home-jie">
                <div class="fly-panel">
                    <h3 class="fly-panel-title">
                        学并思 最近的提问
                    </h3>
                    <ul class="jie-row">
                        <li>
                            <span class="fly-jing">
                                精
                            </span>
                            <a href="/jie/6695.html" class="jie-title">
                                分享自己的一点劳动成果
                            </a>
                            <i>
                                2017-01-11
                            </i>
                            <em>
                                874阅/12答
                            </em>
                        </li>
                        <li>
                            <a href="/jie/6683.html" class="jie-title">
                                这个插件有没有多图同时上传插件
                            </a>
                            <i>
                                2017-01-10
                            </i>
                            <em>
                                251阅/2答
                            </em>
                        </li>
                        <li>
                            <a href="/jie/4418.html" class="jie-title">
                                刚开始用这个哪里有好的视频教程
                            </a>
                            <i>
                                2016-10-20
                            </i>
                            <em>
                                443阅/2答
                            </em>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="layui-inline fly-home-da">
                <div class="fly-panel">
                    <h3 class="fly-panel-title">
                        学并思 最近的回答
                    </h3>
                    <ul class="home-jieda">
                        <li>
                            <p>
                                <span>
                                    2017-04-07 14:28:10
                                </span>
                                在
                                <a href="/jie/9052.html#item-1491546490844" target="_blank">
                                    弹窗用不了，是怎么回事？？
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                <img src="https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_ca79a146.png">
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-04-06 21:42:52
                                </span>
                                在
                                <a href="/jie/6034.html#item-1491486172907" target="_blank">
                                    富文本工具，只能获取文本，自己加了一句代码实现修改文本，分享给大家
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                @
                                <a href="javascript:;" class="fly-aite">
                                    字恋狂
                                </a>
                                sgfsfsdf
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-04-06 21:41:32
                                </span>
                                在
                                <a href="/jie/6034.html#item-1491486092064" target="_blank">
                                    富文本工具，只能获取文本，自己加了一句代码实现修改文本，分享给大家
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                <img alt="[嘻嘻]" title="[嘻嘻]" src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/0b/tootha_thumb.gif">
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-04-06 11:24:18
                                </span>
                                在
                                <a href="/jie/6695.html#item-1491449058306" target="_blank">
                                    分享自己的一点劳动成果
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                @
                                <a href="javascript:;" class="fly-aite">
                                    学并思
                                </a>
                                刷新 一下
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-03-23 08:45:40
                                </span>
                                在
                                <a href="/jie/8449.html#item-1490229940491" target="_blank">
                                    你的案例或技术分享，将会被置顶推荐
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                @
                                <a href="javascript:;" class="fly-aite">
                                    犯二青年
                                </a>
                                雨辰
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-01-16 10:43:39
                                </span>
                                在
                                <a href="/jie/6695.html#item-1484534619280" target="_blank">
                                    分享自己的一点劳动成果
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                @
                                <a href="javascript:;" class="fly-aite">
                                    mylxnet
                                </a>
                                目前里面的数据是写死的，没有动态获取，可以结合自己的系统自己附数据上去
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-01-16 10:42:55
                                </span>
                                在
                                <a href="/jie/6695.html#item-1484534575811" target="_blank">
                                    分享自己的一点劳动成果
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                @
                                <a href="javascript:;" class="fly-aite">
                                    S.Jone
                                </a>
                                我的那分页只是js效果，并没有实时异步请求数据，这个需要自己去实现
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-01-11 14:38:43
                                </span>
                                在
                                <a href="/jie/6695.html#item-1484116723546" target="_blank">
                                    分享自己的一点劳动成果
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                @
                                <a href="javascript:;" class="fly-aite">
                                    單克拉的眼淚
                                </a>
                                非常不错，手机端的应该不会考虑，因为后台数据表格展现的数据在手机不好搞。其它找个时间完善下，非常感谢提的建议。
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-01-11 11:25:17
                                </span>
                                在
                                <a href="/jie/6695.html#item-1484105117501" target="_blank">
                                    分享自己的一点劳动成果
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                @
                                <a href="javascript:;" class="fly-aite">
                                    S.Jone
                                </a>
                                样式基本都是用layui上的
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-01-10 16:43:01
                                </span>
                                在
                                <a href="/jie/6683.html#item-1484037781040" target="_blank">
                                    这个插件有没有多图同时上传插件
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                基于Laytpl + Laypage 封装的一个动态分页组件，能过ajax方式获取数据
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2017-01-07 09:11:10
                                </span>
                                在
                                <a href="/jie/6591.html#item-1483751470145" target="_blank">
                                    select多选的时候有问题
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                @
                                <a href="javascript:;" class="fly-aite">
                                    StormerZ
                                </a>
                                这个可以有
                            </div>
                        </li>
                        <li>
                            <p>
                                <span>
                                    2016-10-20 17:27:36
                                </span>
                                在
                                <a href="/jie/4418.html#item-1476955656011" target="_blank">
                                    刚开始用这个哪里有好的视频教程
                                </a>
                                中回答：
                            </p>
                            <div class="home-dacontent">
                                我看下自己可以回答吗
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer">
            <p>
                <a href="http://fly.layui.com/">
                    Fly社区
                </a>
                2017 &copy;
                <a href="http://www.layui.com/">
                    layui.com
                </a>
            </p>
            <p>
                <a href="http://fly.layui.com/jie/3147.html" target="_blank">
                    产品授权
                </a>
                <a href="http://fly.layui.com/jie/8157.html" target="_blank">
                    获取Fly社区模版
                </a>
                <a href="http://fly.layui.com/jie/2461.html" target="_blank">
                    微信公众号
                </a>
            </p>
        </div>
    </body>
</html>