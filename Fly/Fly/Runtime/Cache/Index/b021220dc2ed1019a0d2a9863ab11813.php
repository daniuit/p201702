<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>
            <?php echo $title ?>-<?php echo C('SITE_NAME') ?>
        </title>
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/font.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/layui.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/global.css">
        <script src="/P201702/Fly/Public/Index/layui.js"></script>
    </head>
    
    <body>
        <div class="header">
            <div class="main">
                <a class="logo" href="/P201702/Fly" title="Fly">
                    Fly社区
                </a>
                <div class="nav">
                    <a href="/jie/">
                        <i class="iconfont icon-wenda">
                        </i>
                        讨论
                    </a>
                    <a href="/case/2017/">
                        <i class="iconfont icon-iconmingxinganli" style="top: 2px;">
                        </i>
                        案例
                    </a>
                    <a href="http://www.layui.com/">
                        <i class="iconfont icon-ui">
                        </i>
                        框架
                    </a>
                </div>
                <?php if(isset($_SESSION['uid'])){ ?>
                    <div class="nav-user">
                    <a class="avatar" href="/user/">
                        <img id="face" src="<?php echo $_SESSION['face'] ?>">
                        <cite>
                            <?php echo $_SESSION['nickname'] ?>
                        </cite>
                    </a>
                    <div class="nav">
                        <a href="/user/set/">
                            <i class="iconfont icon-shezhi">
                            </i>
                            设置
                        </a>
                        <a href="<?php echo U('index/login/out');?>">
                            <i class="iconfont icon-tuichu" style="top: 0; font-size: 22px;">
                            </i>
                            退了
                        </a>
                    </div>
                </div>
                <?php }else{ ?>

               
                <div class="nav-user">
                    <a class="unlogin" href="<?php echo U('index/login/index');?>">
                        <i class="iconfont icon-touxiang">
                        </i>
                    </a>
                    <span>
                        <a href="<?php echo U('index/login/index');?>">
                            登入
                        </a>
                        <a href="<?php echo U('index/reg/index');?>">
                            注册
                        </a>
                    </span>
                    <p class="out-login">
                        <a href="<?php echo U('index/login/qqlogin');?>" 
                        class="iconfont icon-qq" title="QQ登入">
                        </a>
                        <a href="http://fly.layui.com:8098/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
                        class="iconfont icon-weibo" title="微博登入">
                        </a>
                    </p>
                </div>

                 <?php } ?>
                
            </div>
        </div>
        <div class="main layui-clear">
    <div class="wrap">
        <div class="content">
            <div class="fly-tab fly-tab-index">
                <span>
                    <a href="/jie/">
                        全部
                    </a>
                    <a href="/jie/unsolved/">
                        未结帖
                    </a>
                    <a href="/jie/solved/">
                        已采纳
                    </a>
                    <a href="/jie/wonderful/">
                        精帖
                    </a>
                    <a href="/user/">
                        我的帖
                    </a>
                </span>
                <form action="<?php echo U('index/so/index');?>" class="fly-search">
                    <i class="iconfont icon-sousuo">
                    </i>
                    <input class="layui-input" autocomplete="off" placeholder="搜索内容，回车跳转"
                    type="text" name="q">
                </form>
                <a href="<?php echo U('index/jie/add');?>" class="layui-btn jie-add">
                    发表新帖
                </a>
            </div>
            <ul class="fly-panel fly-list fly-list-top">
                <?php foreach ($topData as $row) { ?>
                    
                
                <li class="fly-list-li">
                    <a href="<?php echo U('index/U/index',array('id'=>$row['uid']));?>" class="fly-list-avatar">
                        <img src="//cdn.layui.com/avatar/168.jpg?t=1490352249902" alt="贤心">
                    </a>
                    <h2 class="fly-tip">
                        <a href="<?php echo U('index/jie/index',array('id'=>$row['id']));?>">
                            <?php echo $row['title'] ?>
                        </a>
                        <span class="fly-tip-stick">
                            置顶
                        </span>
                        <?php if($row['jing']==1){ ?>
                            <span class="fly-tip-jing">精帖</span>
                        <?php } ?>
                    </h2>
                    <p>
                        <span>
                            <a href="/u/168">
                                <?php echo $row['nickname'] ?>
                            </a>
                        </span>
                        <span>
                            <?php echo Ctime($row['create_time']) ?>
                        </span>
                        <span>
                            <?php echo $row['name'] ?>
                        </span>
                        <span class="fly-list-hint">
                            <i class="iconfont" title="回答">
                                
                            </i>
                            <?php echo $row['answer_num'] ?>
                            <i class="iconfont" title="人气">
                                
                            </i>
                            3<?php echo $row['view_num'] ?>
                        </span>
                    </p>
                </li>
                <?php } ?>
            </ul>
            <ul class="fly-panel fly-list">
                <?php foreach ($allData as $row) { ?>
                    
                
                <li class="fly-list-li">
                    <a href="<?php echo U('index/U/index',array('id'=>$row['uid']));?>" class="fly-list-avatar">
                        <img src="//cdn.layui.com/avatar/168.jpg?t=1490352249902" alt="贤心">
                    </a>
                    <h2 class="fly-tip">
                        <a href="/P201702/Fly/jie/<?php echo $row['id'] ?>.html">
                            <?php echo $row['title'] ?>
                        </a>
                        <?php if($row['jing']==1){ ?>
                            <span class="fly-tip-jing">精帖</span>
                        <?php } ?>
                    </h2>
                    <p>
                        <span>
                            <a href="/u/168">
                                <?php echo $row['nickname'] ?>
                            </a>
                        </span>
                        <span>
                            <?php echo Ctime($row['create_time']) ?>
                        </span>
                        <span>
                            <?php echo $row['name'] ?>
                        </span>
                        <span class="fly-list-hint">
                            <i class="iconfont" title="回答">
                                
                            </i>
                            <?php echo $row['answer_num'] ?>
                            <i class="iconfont" title="人气">
                                
                            </i>
                            3<?php echo $row['view_num'] ?>
                        </span>
                    </p>
                </li>
                <?php } ?>
            </ul>
            <style type="text/css">
                .laypage-main{
                    border: 0px;
                }
                .laypage-main div{
                    border: 0px;
                }
                .laypage-main .num{
                    margin: 0px 10px;
                    border: 1px solid #009E94;
                }

                .laypage-main .current{
                    margin: 0px 10px;
                    border: 1px solid #009E94;
                    background: #009E94 url() 0 0 no-repeat;
                    color: #fff;
                }


                .laypage-main .next,.laypage-main .prev,.laypage-main .end{
                    margin: 0px 10px;
                    border: 1px solid #009E94;
                }


            </style>
            <div style="text-align: center">
                <div class="laypage-main">
                    <?php echo $show; ?>  
                </div>
            </div>
        </div>
    </div>
    <div class="edge">
        <div class="fly-panel leifeng-rank">
            <h3 class="fly-panel-title">
                近一月回答榜 - TOP 12
            </h3>
            <dl>
                <?php foreach ($hotuser as $row) { ?>
                <dd>
                    <a href="<?php echo U('index/U/index',array('id'=>$row['id']));?>">
                        <img src="/P201702/Fly<?php echo $row['face'] ?>">
                        <cite>
                            <?php echo $row['nickname'] ?>
                        </cite>
                        <i>
                            <?php echo $row['answer_num'] ?>次回答
                        </i>
                    </a>
                </dd>
                <?php } ?>
            </dl>
        </div>
        <dl class="fly-panel fly-list-one">
            <dt class="fly-panel-title">
                最近热帖
            </dt>
            <?php foreach ($hotview as $row) { ?>
            <dd>
                <a href="<?php echo U('index/jie/index',array('id'=>$row['id']));?>">
                    <?php echo $row['title'] ?>
                </a>
                <span>
                    <i class="iconfont">
                        &#xe60b;
                    </i>
                     <?php echo $row['view_num'] ?>
                </span>
            </dd>
            <?php } ?>
        </dl>
        <dl class="fly-panel fly-list-one">
            <dt class="fly-panel-title">
                近期热议
            </dt>
            <dd>
                <a href="/jie/10355.html">
                    弹出层不能置顶！！
                </a>
                <span>
                    <i class="iconfont">
                        
                    </i>
                    12
                </span>
            </dd>
        </dl>
        <div class="fly-panel fly-link">
            <h3 class="fly-panel-title">
                友情链接
            </h3>
            <dl>
                <dd>
                    <a href="http://layim.layui.com/" target="_blank">
                        LayIM
                    </a>
                </dd>
                <dd>
                </dd>
                <dd>
                    <a href="http://layer.layui.com/" target="_blank">
                        layer
                    </a>
                </dd>
                <dd>
                </dd>
                <dd>
                    <a href="http://www.ttlutuan.com" target="_blank">
                        天天撸团
                    </a>
                </dd>
                <dd>
                </dd>
                <dd>
                    <a href="http://www.hotcn.top/" target="_blank">
                        国际热点
                    </a>
                </dd>
                <dd>
                </dd>
                <dd>
                    <a href="http://www.bejson.com/" target="_blank">
                        JSON在线工具
                    </a>
                </dd>
                <dd>
                </dd>
                <dd>
                    <a href="http://www.smeoa.com/" target="_blank">
                        小微OA
                    </a>
                </dd>
                <dd>
                </dd>
                <dd>
                    <a href="http://www.hibug.cn/" target="_blank">
                        在线Bug管理
                    </a>
                </dd>
                <dd>
                </dd>
                <dd>
                    <a href="http://www.houchaowei.com/" target="_blank">
                        MonkeyBlog
                    </a>
                </dd>
                <dd>
                </dd>
            </dl>
        </div>
    </div>
</div>
        <div class="footer">
            <p>
                <a href="http://fly.layui.com/">
                    Fly社区
                </a>
                2017 &copy;
                <a href="http://www.layui.com/">
                    layui.com
                </a>
            </p>
            <p>
                <a href="http://fly.layui.com/jie/3147.html" target="_blank">
                    产品授权
                </a>
                <a href="http://fly.layui.com/jie/8157.html" target="_blank">
                    获取Fly社区模版
                </a>
                <a href="http://fly.layui.com/jie/2461.html" target="_blank">
                    微信公众号
                </a>
            </p>
        </div>
    </body>
</html>