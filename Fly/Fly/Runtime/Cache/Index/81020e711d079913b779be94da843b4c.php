<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>
            <?php echo $title ?>-<?php echo C('SITE_NAME') ?>
        </title>
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/font.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/layui.css">
        <link rel="stylesheet" href="/P201702/Fly/Public/Index/css/global.css">
        <script src="/P201702/Fly/Public/Index/layui.js"></script>
    </head>
    
    <body>
        <div class="header">
            <div class="main">
                <a class="logo" href="/P201702/Fly" title="Fly">
                    Fly社区
                </a>
                <div class="nav">
                    <a href="/jie/">
                        <i class="iconfont icon-wenda">
                        </i>
                        讨论
                    </a>
                    <a href="/case/2017/">
                        <i class="iconfont icon-iconmingxinganli" style="top: 2px;">
                        </i>
                        案例
                    </a>
                    <a href="http://www.layui.com/">
                        <i class="iconfont icon-ui">
                        </i>
                        框架
                    </a>
                </div>
                <?php if(isset($_SESSION['uid'])){ ?>
                    <div class="nav-user">
                    <a class="avatar" href="/user/">
                        <img id="face" src="<?php echo $_SESSION['face'] ?>">
                        <cite>
                            <?php echo $_SESSION['nickname'] ?>
                        </cite>
                    </a>
                    <div class="nav">
                        <a href="/user/set/">
                            <i class="iconfont icon-shezhi">
                            </i>
                            设置
                        </a>
                        <a href="<?php echo U('index/login/out');?>">
                            <i class="iconfont icon-tuichu" style="top: 0; font-size: 22px;">
                            </i>
                            退了
                        </a>
                    </div>
                </div>
                <?php }else{ ?>

               
                <div class="nav-user">
                    <a class="unlogin" href="<?php echo U('index/login/index');?>">
                        <i class="iconfont icon-touxiang">
                        </i>
                    </a>
                    <span>
                        <a href="<?php echo U('index/login/index');?>">
                            登入
                        </a>
                        <a href="<?php echo U('index/reg/index');?>">
                            注册
                        </a>
                    </span>
                    <p class="out-login">
                        <a href="<?php echo U('index/login/qqlogin');?>" 
                        class="iconfont icon-qq" title="QQ登入">
                        </a>
                        <a href="http://fly.layui.com:8098/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
                        class="iconfont icon-weibo" title="微博登入">
                        </a>
                    </p>
                </div>

                 <?php } ?>
                
            </div>
        </div>
        <div class="main fly-user-main layui-clear">
    <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
    <li class="layui-nav-item">
      <a href="<?php echo U('index/u/index');?>">
        <i class="layui-icon">&#xe609;</i>
        我的主页
      </a>
    </li>
    <li class="layui-nav-item <?php if(ACTION_NAME=='index'){echo 'layui-this';} ?>">
      <a href="<?php echo U('index/user/index');?>">
        <i class="layui-icon">&#xe612;</i>
        用户中心
      </a>
    </li>
    <li class="layui-nav-item <?php if(ACTION_NAME=='set'){echo 'layui-this';} ?>">
      <a href="<?php echo U('index/user/set');?>">
        <i class="layui-icon">&#xe620;</i>
        基本设置
      </a>
    </li>
    <li class="layui-nav-item <?php if(ACTION_NAME=='msg'){echo 'layui-this';} ?>" >
      <a href="<?php echo U('index/user/msg');?>">
        <i class="layui-icon">&#xe611;</i>
        我的消息
      </a>
    </li>
  </ul>
    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">
            &#xe602;
        </i>
    </div>
    <div class="site-mobile-shade">
    </div>
    <div class="fly-panel fly-panel-user" pad20>
        <div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_uc">
            <div class="fly-msg">
                您的邮箱尚未验证，这比较影响您的帐号安全，
                <a href="/user/activate/">
                    立即去激活？
                </a>
            </div>
            <ul class="layui-tab-title" id="LAY_mine">
                <li data-type="mine-jie" lay-id="index" class="layui-this">
                    我发的帖（
                    <span>
                        0
                    </span>
                    ）
                </li>
                <li data-type="collection" data-url="/collection/find/" lay-id="collection">
                    我收藏的帖（
                    <span>
                        0
                    </span>
                    ）
                </li>
            </ul>
            <div class="layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                <div class="layui-tab-item layui-show">
                    <ul class="mine-view jie-row">
                    </ul>
                    <div id="LAY_page">
                    </div>
                </div>
                <div class="layui-tab-item">
                    <ul class="mine-view jie-row">
                    </ul>
                    <div id="LAY_page1">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <div class="footer">
            <p>
                <a href="http://fly.layui.com/">
                    Fly社区
                </a>
                2017 &copy;
                <a href="http://www.layui.com/">
                    layui.com
                </a>
            </p>
            <p>
                <a href="http://fly.layui.com/jie/3147.html" target="_blank">
                    产品授权
                </a>
                <a href="http://fly.layui.com/jie/8157.html" target="_blank">
                    获取Fly社区模版
                </a>
                <a href="http://fly.layui.com/jie/2461.html" target="_blank">
                    微信公众号
                </a>
            </p>
        </div>
    </body>
</html>