<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            X-admin v1.0
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="/P201702/Fly/Public/Admin/css/x-admin.css" media="all">
        <script src="/P201702/Fly/Public/Admin/lib/layui/layui.js" charset="utf-8"></script>
    </head>
    <body>
        <div class="x-body">
    <blockquote class="layui-elem-quote">
        <img src="/P201702/Fly/<?php echo $user['face'] ?>" class="layui-circle" style="width:50px;float:left">
        <dl style="margin-left:80px; color:#019688">
        <dt><span ><?php echo $user['nickname'] ?></span> <span >飞吻：<?php echo $user['kiss'] ?></span></dt>
        <dd style="margin-left:0"><?php echo $user['sign'] ?></dd>
      </dl>
    </blockquote>
    <div class="pd-20">
      <table  class="layui-table" lay-skin="line">
        <tbody>
          <tr>
            <th  width="80">性别：</th>
            <td><?php echo $user['sex'] ?></td>
          </tr>
          <tr>
            <th>邮箱：</th>
            <td><?php echo $user['email'] ?></td>
          </tr>
          <tr>
            <th>地址：</th>
            <td><?php echo $user['city'] ?></td>
          </tr>
          <tr>
            <th>注册时间：</th>
            <td><?php echo date("Y-m-d H:i:s",$user['create_time']) ?></td>
          </tr>
          <tr>
            <th>飞吻：</th>
            <td><?php echo $user['kiss'] ?></td>
          </tr>
        </tbody>
      </table>
    </div>
</div>
    </body>
</html>