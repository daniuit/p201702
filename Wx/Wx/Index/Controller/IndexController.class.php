<?php
namespace Index\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
       $token = $this->get_access_token();
    }

    public function get_access_token()
    {
    	if(S('token')){
    		return S('token');
    	}else{
    		$url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxbac82045a637c02e&secret=7c320fea54e77a01fcc59ff7060b04e2";
    		$data = wcurl($url);
    		S('token',$data['access_token'],7200);
    		return S('token');
    	}
    }


    public function menu()
    {
    	$token = $this->get_access_token();
    	$url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$token;

    	$data = array(
    		'button'=>array(
    			array(
    				'type'=>'click',
          			"name"=>"xxxbbb歌曲",
    				"key"=>"V1001_TODAY_MUSIC"
    				),
    			array(
    				'type'=>'view',
          			"name"=>"百xx度",
    				"url"=>"http://www.baidu.com/"
    				),
    			array(
    				 "name"=>"二级菜单",
    				 'sub_button'=>array(
    				 	array(
    				 		"type"=>"view",
				             "name"=>"SOxxxSO",
				             "url"=>"http://www.soso.com/"
    				 		),
    				 	array(
    				 		"type"=>"view",
				             "name"=>"BBxxxS",
				             "url"=>"http://bbs.xuebingsi.com/"
    				 		),
    				 	array(
    				 		"type"=>"view",
				             "name"=>"BAxxxIDU",
				             "url"=>"http://www.baidu.com/"
    				 		),
    				 	),

    				)
    			)
    		);

	   	$res  = wcurl($url,'post',json_encode($data,JSON_UNESCAPED_UNICODE));

	   	var_dump($res);


    }

    public function sendmsg()
    {
    	$token = $this->get_access_token();
    	$url = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=".$token;


    	$data = array(
    		"touser"=>array(
    			'oF5Z2v5yzYfsCYP2oEDfz4ns94p0',
    			'oF5Z2vyRSy-kodlXyzoCeq2dwr1g',
    			'oF5Z2v5jms7KVSx15qN4sMxYsCMI',
    			'oF5Z2v7HV4UazN-u9RjfpvcONG3s',
    			'oF5Z2vxUmGUUC8eyATN6T3ibpEUU',
    			'oF5Z2vwJc9aeqniw3Ald-ydpyW_M',
    			'oF5Z2v89DeY4Tgv5HGDbGuQXKyTQ'
    		),
    		'msgtype'=>"text",
    		'text'=>array('content'=>"你们都收到没有")
    	);


    	$res  = wcurl($url,'post',json_encode($data,JSON_UNESCAPED_UNICODE));

	   	var_dump($res);
    }

    public function moban()
    {
        $token = $this->get_access_token();
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$token;


        $data = array(
            'touser'=>'oF5Z2v5yzYfsCYP2oEDfz4ns94p0',
            'template_id'=>"WLMoCY8mn0ngYkMtG5wWC7w_lhfGpukrteuzPehJmpM",
            'url'=>"http://www.baidu.com",
            'data'=>array(
                'money'=>array(
                    'value'=>120,
                    'color'=>"#173177"
                    ),
                'num'=>array(
                    'value'=>3,
                    'color'=>"#173177"
                    ),
                'count'=>array(
                    'value'=>360,
                    'color'=>"#173177"
                    ),
                ),
            );

        $res  = wcurl($url,'post',json_encode($data,JSON_UNESCAPED_UNICODE));

        var_dump($res);
    }
}