<?php
namespace Api\Controller;
use Think\Controller;
class CommonController extends Controller {
	public function log($data)
	{

		$data['content'] = json_encode($data,JSON_UNESCAPED_UNICODE);

		$data['ctime'] = time();

		M('log')->add($data);
	}
}