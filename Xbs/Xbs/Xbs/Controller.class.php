<?php 
/**
* 控制器基类
*/
class Controller 
{
	public $data=array();//存储分配过来的数据
	//加载视图
	public function display($tpl_name='')
	{

		// var_dump($this->data);
		// 把数组转成变量
		extract($this->data);

		// 判断是否有传模板名
		if($tpl_name){
			// 引入视图
			include VIEW_PATH.'/'.CONTROLLER_NAME.'/'.$tpl_name.'.html';
		}else{
			// 引入视图
			include  VIEW_PATH.'/'.CONTROLLER_NAME.'/'.ACTION_NAME.'.html';
		}
		
	}
	//分配变量
	public function assign($key,$value)
	{
		$this->data[$key]=$value;
	}
}












 ?>