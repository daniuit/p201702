<?php 

/**
* 上传类
*/
class Upload 
{
	public $config = array(
		'path'=>'./Upload',
		'max_size'=>100000,
		'type'=>['jpg','png','gif']
	);
	public $data;
	public function __construct($config=array())
	{
		if(!empty($config)){
			$this->config = $config;
		}
	}
	
	public function save()
	{
		$this->data = current($_FILES);
		$this->checkError();
		$this->checkSize();
		$this->checkType();
		$this->move();

	}
	private function checkError()
	{
		foreach ($this->data['name'] as $k => $v) {
			if($this->data['error'][$k]!=0){
				echo $this->data['name'][$k]."上传错误：错误代码是：".$this->data['error'][$k];
				echo "<br>";
				unset($this->data['name'][$k]);
			}
		}
	}
	private function checkSize()
	{
		foreach ($this->data['name'] as $k => $v) {
			if($this->data['size'][$k]>$this->config['max_size']){
				echo $this->data['name'][$k]."上传错误：大小超过".$this->config['max_size'];
				echo "<br>";
				unset($this->data['name'][$k]);
			}
		}
	}

	private function checkType()
	{
		foreach ($this->data['name'] as $k => $v) {
			$arr = explode('.',$this->data['name'][$k]);
			$suffix = end($arr);

			$this->data['suffix'][$k]=$suffix;

			if(!in_array($suffix, $this->config['type'])){
				echo $this->data['name'][$k]."上传类型不允许。";
				echo "<br>";
				unset($this->data['name'][$k]);
			}
		}
	}

	private function move()
	{

		foreach ($this->data['name'] as $k => $v) {
			$name = uniqid();

			is_dir($this->config['path']) || mkdir($this->config['path']);

			move_uploaded_file($this->data['tmp_name'][$k],$this->config['path'].'/'.$name.'.'.$this->data['suffix'][$k]);
		}
	}
}













 ?>