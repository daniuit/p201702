<?php 

include '../function.php';
//取第一个子数组
$fileData = current($_FILES);

//判断上传是否有错误
if($fileData['error']!=0){
	echo "上传错误，错误代码是：".$fileData['error'];exit;
}
//判断大小是否符合
if($fileData['size']>100000){
	echo "上传错误，大小超过限制";exit;
}

//获取后缀
$fnameArr = explode('.', $fileData['name']);

$suffix = end($fnameArr);

$allowType= ['jpg','png','gif'];

// 判断后缀是否符合
if(!in_array($suffix, $allowType)){
	echo "上传错误，类型不支持";exit;
}

// 生成文件名
$name = uniqid();

//移动临时文件到目标文件
move_uploaded_file($fileData['tmp_name'], './Upload/'.$name.'.'.$suffix);

echo "上传成功";

