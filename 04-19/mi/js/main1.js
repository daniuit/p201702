$(function  () {
	// 定义控制变量
	num = 0;
	//循环执行的方法
	function run () {
		//让所有的消失
		
		$('.img_list').eq(num).fadeIn().siblings().fadeOut();

		$('.click_list').eq(num).css('background', '#fff').siblings().css('background', '#333');

		// 走到最后一个怎么处理
		if(num==$('.img_list').length-1){
			num=0;
		}else{
			num++;
		}
	}

	// 定时器
	time = setInterval(run, 1000);

	// 进入事件
	$('#banner').hover(function() {
		clearInterval(time);
	}, function() {
		time = setInterval(run, 1000);
	});

	$('.click_list').click(function(event) {
		num = $(this).index();
		run();
	});

	//右运动
	$('#click_right').click(function(event) {
		run();
	});
	//左运动
	$('#click_left').click(function(event) {
		if(num==1){
			num=$('.img_list').length-1;
		}else if(num==0){
			num=$('.img_list').length-2;
		}else{
			num-=2;
		}
		run();
	})

// <---------------------------------------->
	// 购物车
	// 
	$('.car').hover(function() {
		$('.car_son').slideToggle();
	}, function() {
		$('.car_son').slideToggle();
	});

// <---------------------------------------->
	// 明星左右走
	
	function star_run (argument) {
		if($('.star').css('left')=="0px"){
			$('.star').animate({left:"-1226px"}, 500);
		}else{
			$('.star').animate({left:"0px"}, 500);
		}
	}

	setInterval(star_run, 3000);

	//子菜单 
	//
	$('.menu_list li').hover(function() {
		$(this).children('.menu_list_son').show();
	}, function() {
		$(this).children('.menu_list_son').hide();
	});

	$('.header .header_mid .nav .menu a').hover(function() {
		index = $(this).index();
		$('.show .show_list').eq(index).show().siblings().hide();
		$('.show').stop().slideToggle();
	}, function() {
		$('.show').stop().slideToggle();
	});

	$('.show').hover(function() {
		$(this).stop().slideToggle();
	}, function() {
		$(this).stop().slideToggle();
	});


	$(window).scroll( function() {
		sosoTop= $(this).scrollTop();
		if(sosoTop>600){
			$('.soso').slideDown();
		}else{
			$('.soso').slideUp();
		}
	} );

 







})