window.onload = function  () {
	//捉元素
	img_list = document.getElementsByClassName('img_list');
	click_list = document.getElementsByClassName('click_list');
	banner = document.getElementById('banner');
	click_right = document.getElementById('click_right');
	// 定义控制变量
	num = 0;
	//循环执行的方法
	function run () {
		//让所有的消失
		for (var i =0; i<img_list.length; i++) {
			img_list[i].style.display="none";
			click_list[i].style.backgroundColor="#333";
		};
		//让某一个出现
		img_list[num].style.display="block";
		click_list[num].style.backgroundColor="#fff";

		//走到最后一个怎么处理
		if(num==img_list.length-1){
			num=0;
		}else{
			num++;
		}
	}

	// 定时器
	time = setInterval(run, 1000);

	// 进入事件
	banner.onmouseover = function  () {
		clearInterval(time);
	}
	// 离开事件
	banner.onmouseout = function  () {
		time = setInterval(run, 1000);
	}

	//循环点加事件
	for (var i = 0; i < click_list.length; i++) {
		add_click(i);
	};

	function add_click (x) {
		click_list[x].onclick = function  () {
			num=x;
			run();
		}
	}
	//右运动
	click_right.onclick = function  () {
		run();
	}
	//左运动
	click_left.onclick = function  () {
		if(num==1){
			num=img_list.length-1;
		}else if(num==0){
			num=img_list.length-2;
		}else{
			num-=2;
		}
		run();
	}

}